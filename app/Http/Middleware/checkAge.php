<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class checkAge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     * : //Response
     */
    public function handle(Request $request, Closure $next)
    {
        // echo "global message for all page";
        if ($request->age && $request->age < 18) {
            return redirect('noaccess');
        }
        return $next($request);
    }
}
