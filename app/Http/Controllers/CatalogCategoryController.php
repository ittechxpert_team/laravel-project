<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Result;
use App\Models\CatalogCategory;


class CatalogCategoryController extends Controller
{
    public function list(){
        // return Result::all();
        return CatalogCategory::all();
        // return DB::table('results')->get();
        // return DB::connection('mysql2')->table('catalog_categorys')->get();
    }
}
