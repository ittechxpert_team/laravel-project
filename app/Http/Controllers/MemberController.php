<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Models\User;
use Illuminate\Support\Facades\DB;

class MemberController extends Controller
{
    // function list(){
    //     $date =  User::all();
    //     return view('list',['users' => $date]);
    //     // return view('list');
    // }
    // function delete($id){
    //     $date = User::find($id);
    //     $date->delete();
    //     return redirect('list');
    // }
    // //  function show(){
    // //     $date =  User::all();
    // //     return view('list',['users' => $date]);
    // // }
    // function addData(Request $req){
    //     //     $date =  User::paginate(10);
    //     //     return view('lis t',['users' => $date]);
    //     $member = new user;
    //     $member->fname=$req->fname;
    //     $member->lname=$req->lname;
    //     $member->email=$req->email;
    //     $member->pass=$req->pass;
    //     $member->age=$req->age;
    //     $member->save();
    //     return redirect('add');
    // }

    // function showData($id){
    // $date =  User::find($id);
    //     return view('edit',['data' => $date]);
    // }

    // function updata(Request $req){
    //     $data = User::find($req->id);
    //     $data->fname=$req->fname;
    //     $data->lname=$req->lname;
    //     $data->email=$req->email;
    //     // $data->pass=$req->pass;
    //     $data->age=$req->age;
    //     $data->save();
    //     return redirect('list');
    // }

//  Query Builder 
    
    function operations(){
        return DB::table('users')->get();

        // Aggregate methods count data with qurey
        // return DB::table('users')->sum('id');
        // return DB::table('users')->min('id');
        // return DB::table('users')->max('id');
        // return DB::table('users')->avg('id');

        // Delete data with qurey
        // return DB::table('users')->where('id', 254)->delete();
        // // Update data with qurey
        // return DB::table('users')->where('id', 254)->update([
        //     'fname'=>'Kavita',
        //     'lname'=>'Rani',
        //     'email'=>'kavitarani@gmail.com',
        //     'pass'=>12345,
        //     'age'=>220851655,
        // ]);
        // insert data with qurey

        // return DB::table('users')->insert([
        //     'fname'=>'Kavita',
        //     'lname'=>'Rani',
        //     'email'=>'kavita@gmail.com',
        //     'pass'=>'12345',
        //     'age'=>'220851655',
        // ]);

        // return DB::table('users')->get();
        // return DB::table('users')->count();
        // return (array)DB::table('users')->find(5);
        // return DB::table('users')->where('id',5)->get();
        // return view('list' , ['data' => $data]); 
    }
     
}


