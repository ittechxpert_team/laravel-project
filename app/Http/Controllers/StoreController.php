<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Storecontroller extends Controller
{
    public function storeM(Request $req){
        $data =  $req->input('user');
        $req->session()->flash('user',$data);
        return redirect('store');
    }
}
