<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class JoinController extends Controller
{
    public function getData(){
        return DB::table('orders')->join('orders_vendors','orders.order_id','=','orders_vendors.order_id')->where('orders.store_name' , 'Wrist-Band.Com')->get();
        // return DB::table('orders')->join('orders_vendors','orders.order_id','=','orders_vendors.order_id')->select('orders.*','orders_vendors.*')->get();
    }
}
