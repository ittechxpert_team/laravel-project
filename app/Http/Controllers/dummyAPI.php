<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Result;
use Validator;


class dummyAPI extends Controller
{
    function index(){
        return ['fname'=>'Ramesh','lname'=>'kumar','email'=>'rameshkumar@gmail.com','pass'=>123];
    }
    // list show
    function list($id=null){

        return $id?Result::find($id):Result::all();
    }
    // add data with database in api
    function add(Request $req){
        $data = new Result;
        $data->name=$req->name; 
        $data->percentage=$req->percentage; 
        $data->age=$req->age; 
        $data->gender=$req->gender; 
        $data->city=$req->city; 
        $result=$data->save();
        if($result){
            return ['Result'=> "Data has been saved"];
        }else{
            return ['Result'=> "Operation Faild"];
        }
    }
    // update data with database in api 
    function update(Request $req){

        $data = Result::find($req->id);
        $data->name=$req->name; 
        $data->percentage=$req->percentage; 
        $data->age=$req->age; 
        $data->gender=$req->gender; 
        $data->city=$req->city; 
        $result=$data->save();
        if($result){
            return ['Result'=> "Data has been Updated"];
        }else{
            return ['Result'=> "Updated Operation Faild"];
        }
    }
    // delete data with database in api
    function delete($id){
        $data = Result::find($id);
        $result = $data->delete();
        if($result){
            return ['result'=>'record has been delete'];
        }else{
            return ['Result'=> "Delete Operation Faild"];

        }
    }
    // search data with database in api 
    function search($name){
        return Result::where('name','like'  ,'%'. $name .'%')->get();
    }
    //  API Validation
    function testAPIData(Request $req){
        $rules = array(
            'name'=>'required',
            'age'=>'required | max:3',
            'percentage' =>'required',
            'gender'=>'required',
            'city'=>'required'
        );
        
        $validator = Validator::make($req->all(),$rules);
        if($validator->fails()){
            return response()->json($validator->errors(),401);
            // return $validator->errors();
        }else{

        $data = new Result;
        $data->name=$req->name; 
        $data->percentage=$req->percentage; 
        $data->age=$req->age; 
        $data->gender=$req->gender; 
        $data->city=$req->city; 
        $result=$data->save();
        if($result){
            return ['Result'=> "Data has been saved"];
        }else{
                return ['Result'=> "Operation Faild"];
        }
        }
    }
    // API with Resource
}
