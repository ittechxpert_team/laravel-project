<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Header extends Component
{
    /**
     * Create a new component instance.
     */
    public $title = "";
    public function __construct($componentName)
    {
        //
        $this->title = $componentName;
    }

    /**
     * Get the view / contents that represent the component.
     */
    //View|Closure|string

    public function render()
    {
        return view('components.header');
    }
}
