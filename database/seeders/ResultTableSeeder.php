<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class ResultTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        DB::table('results')->insert([
            'name' => 'John Doe',
            'percentage' => 87,
            'age' =>23,
            'gender' => 'm',
            'city' => 3
        ]);
    }
}
