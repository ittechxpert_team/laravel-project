<?php

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
// use App\Http\Controllers\CarsController;
// use App\Http\Controllers\UsersController;
// use App\Http\Controllers\FormController;
// use App\Http\Controllers\typecontroller;
// use App\Http\Controllers\UserAuth;
// use App\Http\Controllers\StoreController;
// use App\Http\Controllers\UploadController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\JoinController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\CatalogCategoryController;

use App\Mail\SampleMail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/{name}', function ($name) {
//     // echo  $name;
//     return view('welcome', [
//         'name' => $name
//     ]);
// });

// $info =  "hi, let's learn laravel";
// $info = Str::ucfirst($info);
// $info = Str::replaceFirst("Hi","Hello",$info);
// $info = Str::camel($info);
// $info = Str::of($info)->ucfirst($info)->replaceFirst("Hi","Hello",$info)->camel($info);
// echo $info;
Route::get('/', function () {
    return new SampleMail();
    // return view('welcome');
    // return redirect("/about");
});

Route::get('/profile/{lang}', function ($lang) {
    App::setlocale($lang);
    return view('profile');
    // return redirect("/about");
});
Route::get('list',[MemberController::class , 'operations']);
Route::get('testlist',[CatalogCategoryController::class,'list']);
Route::get('join',[JoinController::class , 'getData']);
Route::get('data/{key:name}',[OrdersController::class , 'index']);
// Route::get('order',[OrdersController::class , 'index']);

// Route::get('list',[MemberController::class , 'list']);
// Route::get('delete/{id}',[MemberController::class , 'delete']);
// Route::get('edit/{id}',[MemberController::class , 'showData']);
// Route::post('edit',[MemberController::class , 'updata']);

// Route::get('users',[UsersController::class , 'getAllData']);
// Route::get('users',[UsersController::class , 'index']);

// Route::view('login', 'users' );
// Route::delete('users',[UsersController::class , 'testRequest']);
// Route::view('login',"login");

Route::get('/logout', function () {
   if(session()->has('user')){
        session()->pull('user',null);
   }
    return redirect('login');
});  

Route::get('/login', function () {
   if(session()->has('user')){
    return redirect('profile');
   }
    return view('login');

});

// Route::post("user",[UserAuth::class,"userLogin"]);
// Route::post('add',[MemberController::class,"addData"]);
// Route::post("add",[MemberController::class,"addData"]);
// Route::view('add','addmember');

// Route::post('upload',[UploadController::class,'index']);
Route::view('upload','upload');
Route::view('/store', 'storeuser');
// Route::post('storecontroller',[StoreController::class,'storeM']);
// Route::view('/profile', 'profile');
Route::view('/about', 'about');
Route::view('/home', 'home');
// Route::view('/hello', 'hello');
Route::view('/noaccess', 'noaccess');
// Route::view('/users', 'users')->middleware('protectedPage');

// Route::get('welcome/{name}',[typecontroller::class,"loadviewindex"]);

// Route::group(['middleware' => ['protectPage']], function () {
//     Route::view('users', 'users');
// });


// Route::get("cars/{id} ", [CarsController::class, "index"]);
// Route::get("view", [UsersController::class, 'loadView']);

// Route::post('/form', [FormController::class, "getDate"]);
// Route::view('login', 'form');
