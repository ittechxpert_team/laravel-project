<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\dummyAPI;
use App\Http\Controllers\OrderAPIController;
use App\Http\Controllers\FileController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('test' , [dummyAPI::class,'index']);
Route::get('list/{id?}' , [dummyAPI::class,'list']);
Route::post('add' ,[dummyAPI::class,'add']);
Route::put('update' ,[dummyAPI::class,'update']);
Route::delete('delete/{id}' ,[dummyAPI::class,'delete']);
Route::get('search/{name}' ,[dummyAPI::class,'search']);
Route::post('validator' ,[dummyAPI::class,'testAPIData']);


Route::post('upload',[FileController::class , 'upload']); 
Route::apiResource('Member' , OrderAPIController::class);